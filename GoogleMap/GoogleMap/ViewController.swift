//
//  ViewController.swift
//  GoogleMap
//
//  Created by Ashwini Bankar on 05/08/19.
//  Copyright © 2019 Ashwini Bankar. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    static let GOOGLE_MAP_KEY = "enter_your_google_key"

    @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        googleDir()
    }

    func googleDir(){
        let origin = "\(19.191002701427),\(72.9762332048947)"
        let destination = "\(19.1953664459467),\(72.9736535065556)"
         let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(ViewController.GOOGLE_MAP_KEY)"
        self.getData(url: url, headers: ["":""], selector: #selector(polylineOnMap), handler: self, showIndicatior: false)
    }
    
    //MARK: - Http Get Method
     func getData(url: String, headers: HTTPHeaders, selector: Selector, handler: Any, showIndicatior: Bool){
        
        Alamofire.request(url).responseJSON { response in
            do {
                if let json : [String:Any] = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String: Any]{
                    
                    let routes = json["routes"] as? NSArray
                    _ = (handler as AnyObject).perform(selector, with: routes)
                    
                }
                
            }catch{
                print("error in JSONSerialization")
            }
        }
    }
    
    @objc func polylineOnMap(response: NSArray){
        for route in response {
            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
            let points = routeOverviewPolyline.object(forKey: "points")
            if let path = points as? String{
                let path = GMSPath.init(fromEncodedPath: path)
                
                let polyline = GMSPolyline(path: path)
                polyline.strokeColor = .red
                polyline.strokeWidth = 3.0
                polyline.map = self.mapView
            }
            
        }
        
    }
}

