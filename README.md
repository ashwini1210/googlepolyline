# GooglePolyline

## Summery

Draw a polyline on the Google Map using `GMSPolyline` object that represents an ordered sequence of locations,
displayed as a series of line segments.

## Version

This app is developed for iPhone using `Swift 4.2`

## Build and Runtime Requirements

1. Xcode 10.0 or later
2. iOS 12 or later
3. OS X v10.14 or later

## Configuring the Project

1. Clone the repository 
2. command 'pod install' on the Terminal
3. Open GoogleMap.xcworkspace

## Pods:

1. Alamofire
2. SwiftyJSON
3. GoogleMaps
4. GooglePlaces


## Installation:

Replace your Google Maps key in AppDelegate.swift

>GMSServices.provideAPIKey("YOUR_GOOGLE_MAPS_KEY")
	

## Reference link:

https://developers.google.com/maps/documentation/ios-sdk/shapes

